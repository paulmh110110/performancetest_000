﻿namespace PerformanceTest_000
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;


    public partial class UIMap
    {

        /// <summary>
        /// connecttomattersphere - Use 'KnownEnvironment' to pass parameters into this method.
        /// </summary>
        public void connecttomattersphere(string env)
        {
            #region Variable Declarations
            WinListItem uIBlankdocumentListItem = this.UIWordWindow.UIFeaturedList.UIBlankdocumentListItem;
            WinButton uIConnectButton = this.UIWordWindow.UIItemWindow.UIMatterSphereToolBar.UIConnectButton;
            WinComboBox uICboMultiDBComboBox = this.UIMatterSphereWindow.UICboMultiDBWindow.UICboMultiDBComboBox;
            WinCheckBox uIClearCacheCheckBox = this.UIMatterSphereWindow.UIClearCacheWindow.UIClearCacheCheckBox;
            WinButton uILoginButton = this.UIMatterSphereWindow.UILoginWindow.UILoginButton;
            #endregion

            // Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
            ApplicationUnderTest wINWORDApplication = ApplicationUnderTest.Launch(this.connecttomattersphereParams.ExePath, this.connecttomattersphereParams.AlternateExePath);

            // Click 'Blank document' list item
            Mouse.Click(uIBlankdocumentListItem, new Point(105, 141));

            // Click 'Connect' button
            Mouse.Click(uIConnectButton, new Point(20, 33));

            // Select 'Mattersphere Test' in 'cboMultiDB' combo box
            uICboMultiDBComboBox.SelectedItem = Environments.KnownEnvironment.GetEnvironment();

            // Select 'Clear &Cache' check box
            uIClearCacheCheckBox.Checked = this.connecttomattersphereParams.UIClearCacheCheckBoxChecked;

            // Click 'Login' button
            Mouse.Click(uILoginButton, new Point(33, 11));
        }

        public virtual connecttomattersphereParams connecttomattersphereParams
        {
            get
            {
                if ((this.mconnecttomattersphereParams == null))
                {
                    this.mconnecttomattersphereParams = new connecttomattersphereParams();
                }
                return this.mconnecttomattersphereParams;
            }
        }

        private connecttomattersphereParams mconnecttomattersphereParams;

        /// <summary>
        /// openwordcontomsnologin - Use 'openwordcontomsnologinParams' to pass parameters into this method.
        /// </summary>
        public void openwordcontomsnologin()
        {
            #region Variable Declarations
            WinListItem uIBlankdocumentListItem = this.UIWordWindow.UIFeaturedList.UIBlankdocumentListItem;
            WinButton uIConnectButton = this.UIWordWindow.UIItemWindow.UIMatterSphereToolBar.UIConnectButton;
            WinComboBox uICboMultiDBComboBox = this.UIMatterSphereWindow.UICboMultiDBWindow.UICboMultiDBComboBox;
            WinCheckBox uIClearCacheCheckBox = this.UIMatterSphereWindow.UIClearCacheWindow.UIClearCacheCheckBox;
            #endregion

            // Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
            ApplicationUnderTest wINWORDApplication = ApplicationUnderTest.Launch(this.openwordcontomsnologinParams.ExePath, this.openwordcontomsnologinParams.AlternateExePath);

            // Click 'Blank document' list item
            Mouse.Click(uIBlankdocumentListItem, new Point(83, 173));

            // Click 'Connect' button
            Mouse.Click(uIConnectButton, new Point(31, 30));

            // Select 'Mattersphere UAT (NEW)' in 'cboMultiDB' combo box
            uICboMultiDBComboBox.SelectedItem = Environments.KnownEnvironment.GetEnvironment();

            // Select 'Clear &Cache' check box
            uIClearCacheCheckBox.Checked = this.openwordcontomsnologinParams.UIClearCacheCheckBoxChecked;
        }

        public virtual openwordcontomsnologinParams openwordcontomsnologinParams
        {
            get
            {
                if ((this.mopenwordcontomsnologinParams == null))
                {
                    this.mopenwordcontomsnologinParams = new openwordcontomsnologinParams();
                }
                return this.mopenwordcontomsnologinParams;
            }
        }

        private openwordcontomsnologinParams mopenwordcontomsnologinParams;
    }
    /// <summary>
    /// Parameters to be passed into 'connecttomattersphere'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class connecttomattersphereParams
    {

        #region Fields
        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
        /// </summary>
        public string ExePath = "C:\\Program Files\\Microsoft Office 15\\root\\office15\\WINWORD.EXE";

        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
        /// </summary>
        public string AlternateExePath = "%ProgramW6432%\\Microsoft Office 15\\root\\office15\\WINWORD.EXE";

        /// <summary>
        /// Select 'Mattersphere Test' in 'cboMultiDB' combo box
        /// </summary>
        public string UICboMultiDBComboBoxSelectedItem = "Mattersphere Test";

        /// <summary>
        /// Select 'Clear &Cache' check box
        /// </summary>
        public bool UIClearCacheCheckBoxChecked = true;
        #endregion
    }
    /// <summary>
    /// Parameters to be passed into 'openwordcontomsnologin'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class openwordcontomsnologinParams
    {

        #region Fields
        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
        /// </summary>
        public string ExePath = "C:\\Program Files\\Microsoft Office 15\\root\\office15\\WINWORD.EXE";

        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
        /// </summary>
        public string AlternateExePath = "%ProgramW6432%\\Microsoft Office 15\\root\\office15\\WINWORD.EXE";

        /// <summary>
        /// Select 'Mattersphere UAT (NEW)' in 'cboMultiDB' combo box
        /// </summary>
        public string UICboMultiDBComboBoxSelectedItem = "Mattersphere UAT (NEW)";

        /// <summary>
        /// Select 'Clear &Cache' check box
        /// </summary>
        public bool UIClearCacheCheckBoxChecked = true;
        #endregion
    }
}
