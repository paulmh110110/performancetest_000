﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.Diagnostics;

namespace PerformanceTest_000
{
    /// <summary>
    /// Summary description for MS_PERF_17 - Search for a matter from the Matter List for a specified Fee Earner
    /// </summary>
    [CodedUITest]
    public class MS_PERF_17
    {
        public MS_PERF_17()
        {
        }
        public string GetEnvironment()
        {
            //Gets selected env from teamcity
            var environmentValue = Environment.GetEnvironmentVariable("MSEnvironment");

            {
                //select environment in the KnownEnvironment.cs file
                var env = Environments.KnownEnvironment.GetEnvironment();

            }

            return environmentValue;
        }

        [TestMethod]
        public void MS_PERF17()
        {
            Playback.PlaybackSettings.LoggerOverrideState = HtmlLoggerState.AllActionSnapshot;

            Stopwatch sw2 = new Stopwatch();

            ///open word & connect to environment*********** 
            ///select environment in the KnownEnvironment.cs file
            string envName = GetEnvironment();
            this.UIMap.connecttomattersphere(envName);

            //open mattersphere ribbon
            this.UIMap.openmattersphereribbon();

            //open command centre
            this.UIMap.opencommandcentre();
            System.Threading.Thread.Sleep(3000);

            //open matter list & select fee earner Paul Reddy
            this.UIMap.selectmatterlistselectfe();

            // Set the playback to wait for all threads to finish  
            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.AllThreads;

            //start stopwatch & click search button verify matterlist loaded
            sw2.Start();
            this.UIMap.clicksearchbuttonfepr1();

            //stop stopwatch
            sw2.Stop();

            // Reset the playback to wait only for the UI thread to finish  
            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.UIThreadOnly;

            //close matter & command centre
            this.UIMap.closematterandcs();

            //disconnect form mattersphere & close word
            this.UIMap.disconmatterspherecloseword();

            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.AllThreads;
            var message = string.Format("The application took {0} seconds to run.", sw2.Elapsed);
            Console.WriteLine(message);
            //Log(message);
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
